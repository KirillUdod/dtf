from django.views.generic import TemplateView, View
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import redirect, reverse
from django.core.exceptions import ObjectDoesNotExist

from io import BytesIO

from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

from reportlab.graphics import renderPDF
from reportlab.graphics.charts.lineplots import LinePlot
from reportlab.graphics.charts.textlabels import Label
from reportlab.lib.pagesizes import A4, legal, landscape
from reportlab.graphics.shapes import Drawing
from reportlab.pdfgen import canvas

from pylatex import Document, Section, Subsection, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat
from pylatex.utils import italic

import numpy as np
from matplotlib.lines import Line2D

from stations.models import Station, ModelTask


class StationsView(TemplateView):
    template_name = u'accounts/profile/stations.html'

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self.request.user, u'account'):
            return redirect(reverse(u'index'))
        return super(StationsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(StationsView, self).get_context_data(**kwargs)
        stations = []
        for station in Station.objects.all():
            stations.append({"id": station.id,
                            "name": station.name,
                            "image": station.image,
                            "url": station.get_url()})

        context[u'stations'] = stations
        return context


class ShortStationInfoView(View):
    def get(self, request, **kwargs):
        context = {}
        if request.method == 'GET':
            station = Station.objects.filter(id=request.GET['id']).first()
            # TODO: возможно, требуется поменять на короткое описание
            context['description'] = station.description
            context['duration'] = station.duration
            return JsonResponse(context)


class StationInfoView(TemplateView):
    template_name = u'accounts/profile/station_info.html'

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self.request.user, u'account'):
            return redirect(reverse(u'index'))
        return super(StationInfoView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, guid, **kwargs):
        context = super(StationInfoView, self).get_context_data(**kwargs)
        context[u'back_url'] = self.request.META.get('HTTP_REFERER')
        try:
            context[u'station'] = Station.objects.filter(guid=guid).first()
        except:
            return context
        return context


class ModelingView(TemplateView):
    template_name = u'accounts/profile/modeling.html'

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self.request.user, u'account'):
            return redirect(reverse(u'index'))
        return super(ModelingView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, guid, **kwargs):
        # if id == '1':
        template_name = u'accounts/profile/modeling.html'

        context = super(ModelingView, self).get_context_data(**kwargs)
            # stations = []
            # for station in Station.objects.all():
            #     stations.append({"id": station.id,
            #                     "name": station.name})
            # context[u'stations'] = stations
        return context
        # return HttpResponseRedirect(reverse(u'station_info', kwargs={"id": int(id)}))


class ResultsView(TemplateView):
    template_name = u'accounts/profile/results.html'

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self.request.user, u'account'):
            return redirect(reverse(u'index'))
        return super(ResultsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ResultsView, self).get_context_data(**kwargs)
        context[u'results'] = ModelTask.objects.filter(user=self.request.user.account.id).order_by('-id')
        return context


class GetExcelResultsView(View):

    def get(self, request, guid, **kwargs):

        try:
            arr = ModelTask.objects.get(guid=guid, user=request.user.account).results_data
        except ObjectDoesNotExist:
            return HttpResponse(status=204)

        wb = Workbook()
        ws = wb.active
        ws.title = "results"

        start_col = 1
        start_row = 2
        # writing input values
        j = 0
        _ = ws.cell(column=start_col, row=start_row, value="input")
        start_row += 2
        for key, val in arr['input'].items():
            _ = ws.cell(column=start_col+j, row=start_row, value="{0}".format(key))
            _ = ws.cell(column=start_col+j, row=start_row+1, value="{0}".format(val))
            j += 1

        start_row += 4
        _ = ws.cell(column=start_col, row=start_row, value="results")
        start_row += 2

        # writing output values
        j = 0
        # print(arr['output']['data'])
        for key, val in arr['output']['data'].items():
            # insert name of graph
            _ = ws.cell(column=start_col+j, row=start_row, value="{0}".format(key))
            _ = ws.cell(column=start_col+j, row=start_row+1, value=arr['output']['fields'][0])
            _ = ws.cell(column=start_col+j+1, row=start_row+1, value=arr['output']['fields'][1])

            i = start_row + 2
            for val1 in val:
                _ = ws.cell(column=start_col+j, row=i, value="{0}".format(val1[0]))
                _ = ws.cell(column=start_col+j+1, row=i, value="{0}".format(val1[1]))
                i += 1
            j += 3

        response = HttpResponse(content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename="test.xlsx"'

        buffer = BytesIO(save_virtual_workbook(wb))

        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response


# class GetPDFResultsView(View):


class GetPDFResultsView(View):

    def get(self, request, guid, **kwargs):

        def draw_chart(data):
            drawing = Drawing(125, 300)
            lc = LinePlot()
            lc.y = 50
            lc.height = 100
            lc.width = 300
        # y axis
            lc.data = [[tuple([x, y]) for x, y in data]]
            drawing.add(Label(), name='gr')
            drawing.gr._text = "Graph"
            drawing.gr.textAnchor ='middle'
            drawing.gr.x = 175
            drawing.gr.y = 20
            drawing.gr.fontSize = 12
            drawing.add(lc)
            return drawing

        def draw_plot(data):
            # print(data)
            xdata = [d[0] for d in data]
            ydata = [d[1] for d in data]
            plot = Line2D(xdata,ydata)
            return plot


        try:
            arr = ModelTask.objects.get(guid=guid, user=request.user.account).results_data['output']['data']
        except ObjectDoesNotExist:
            return HttpResponse(status=204)

        # Create the HttpResponse object with the appropriate PDF headers.
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

        buffer = BytesIO()

        # Create the PDF object, using the BytesIO object as its "file."
        size = A4
        p = canvas.Canvas(buffer, pagesize=size)

        # Draw things on the PDF. Here's where the PDF generation happens.
        # See the ReportLab documentation for the full list of functionality.

        start_r = 625
        start_c = 125
        # print(arr)

        for key, value in arr.items():
            # print(key)
            # draw_plot(value)
            renderPDF.draw(draw_chart(value), p, start_c, start_r)
            # renderPDF.draw(draw_plot(value), p, start_c, start_r)
            start_r -= 140

        # Close the PDF object cleanly.
        p.showPage()
        p.save()

        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def get_context_data(self, **kwargs):
        return HttpResponseRedirect(reverse(u'station_info', kwargs={"id": int(guid)}))


# not using
def get_station(request):
    """

    :param request:
    :return:
    """
    if request.method == 'GET':
        context = {}
        station = Station.objects.filter(id=request.GET['id']).first()
        context['description'] = station.description
        context['duration'] = station.duration
        return JsonResponse(context)
