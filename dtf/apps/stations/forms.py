from django import forms
from stations.models import Station

class StationsForm(forms.Form):
    stations = forms.ModelChoiceField(label=u'Stations', required=True, queryset=Station.objects.all())
