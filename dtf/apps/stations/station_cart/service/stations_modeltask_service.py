# -*- coding: utf-8 -*-

__author__ = 'artem'


import json
import psycopg2
import psycopg2.extras


from dao import pool, stations_modeltask_dao

def save_to_db(created, finished, station, user, arr_results, data_input, guid):
    """
    Saved result to DB
    :param created:
    :param finished:
    :param station:
    :param user:
    :param arr_results:
    :param data_input:
    :return:
    """
    ret = {}
    conn = pool.getconn()
    curs = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    try:
        stations_modeltask_dao.save_to_db(curs, created, finished, station, user, arr_results, data_input, guid)
        print('task created %s' % guid)
        conn.commit()
    except psycopg2.Error as e:
        conn.rollback()
        print('save_result %s %s' % (e.pgcode, e.pgerror))
    except Exception as ex:
        conn.rollback()
        print('save_result %s' % ex)
    finally:
        curs.close()
        conn.close()
        pool.putconn(conn)


def update_in_db(finished, arr_results, data_input, guid, status):
    """
    Saved result to DB
    :param finished:
    :param arr_results:
    :param guid:
    :param status:
    :return:
    """
    ret = {}
    conn = pool.getconn()
    curs = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    try:
        stations_modeltask_dao.update_in_db(curs, finished, arr_results, data_input, guid, status)
        print('updated task %s with status %s' % (guid, status))
        conn.commit()
    except psycopg2.Error as e:
        conn.rollback()
        print('update_result %s %s' % (e.pgcode, e.pgerror))
    except Exception as ex:
        conn.rollback()
        print('update_result %s' % ex)
    finally:
        curs.close()
        conn.close()
        pool.putconn(conn)
