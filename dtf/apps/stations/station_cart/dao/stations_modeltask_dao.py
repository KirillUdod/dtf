# -*- coding: utf-8 -*-

__author__ = 'artem'


import json


def save_to_db(cursor, created, finished, station, user, arr_results, data_input, guid):
    """
    Saved result to DB
    :param cursor:
    :param created:
    :param finished:
    :param station:
    :param user:
    :param arr_results:
    :param data_input:
    :param guid:
    :return:
    """
    cursor.execute("""
            INSERT INTO
              stations_modeltask
              (creation_time,
               finish_time,
               results_data,
               station_id,
               user_id,
               guid,
               status
               )
            VALUES
              (
                to_timestamp('{0}', 'dd-mm-yyyy hh24:mi:ss'),
                to_timestamp('{1}', 'dd-mm-yyyy hh24:mi:ss'),
                $${4}$$::JSONB,
                {2},
                {3},
                '{5}',
                {6}
              )
        """.format(created, finished, station,
                   user, json.dumps(dict(input=data_input, output=arr_results), ensure_ascii=False), guid, 1))


def update_in_db(cursor, finished, arr_results, data_input, guid, status):
    """
    Updated result to DB
    :param cursor:
    :param finished:
    :param arr_results:
    :param data_input:
    :param guid:
    :param status:
    :return:
    """
    cursor.execute("""
            UPDATE
                stations_modeltask
            SET
                finish_time = to_timestamp('{0}', 'dd-mm-yyyy hh24:mi:ss'),
                results_data = $${1}$$::JSONB,
                status = {2}
            WHERE
                guid = '{3}'
        """.format(finished, json.dumps(dict(input=data_input, output=arr_results), ensure_ascii=False), status, guid))
