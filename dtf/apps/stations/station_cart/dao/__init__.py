# -*- coding: utf-8 -*-

import psycopg2.pool
import psycopg2.extras

from config import db_config


__author__ = 'artem'


psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)

pool = psycopg2.pool.ThreadedConnectionPool(1, 100, database=db_config['database'], host=db_config['host'],
                                            port=db_config['port'], user=db_config['user'],
                                            password=db_config['password'])
