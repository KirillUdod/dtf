# -*- coding: utf-8 -*-

import serial
import uuid
import json
import time
import _thread

from datetime import datetime
from autobahn.asyncio.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory

from models.RedisQueue import RedisQueue
from config import serial_config
from service import stations_modeltask_service


task = RedisQueue('station_cart')
threads = []
clients = {}

ser = serial.Serial(serial_config['device'], int(serial_config['speed']), timeout=float(serial_config['timeout']))
class MyServerProtocol(WebSocketServerProtocol):

    def __init__(self):
        WebSocketServerProtocol.__init__(self)
        if len(threads) == 0:
            t = _thread.start_new_thread(getData, (self, False))
            threads.append(t)

    def onConnect(self, request):
        print(self.peer, 'connected')
        clients[self.peer] = self

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
        if isBinary:
            print("Binary message received: {0} bytes".format(len(payload)))
        else:
            print("Text message received: {0}".format(payload.decode('utf8')))
        message = json.loads(payload.decode('utf8'))   # message - python string, payload - utf8 encoded json string
        words = message.split(" ")

        if words[0] == 'send_data':
            print('send_data')
            try:
                user = words[1]
                a0 = words[2]
                a1 = words[3]
                a2 = words[4]
                a3 = words[5]
                a4 = words[6]
                a5 = words[7]
                b0 = words[8]
                b1 = words[9]
                b2 = words[10]
                b3 = words[11]
                b4 = words[12]
                b5 = words[13]
                # time.sleep(1)
                station = 1
                creation_time = datetime.now().strftime('%d.%m.%Y %H:%M:%S')
                guid = str(uuid.uuid4())
                stations_modeltask_service.save_to_db(creation_time, datetime(2000, 1, 1, 1, 0, 0, 1).strftime('%d.%m.%Y %H:%M:%S'),
                           station, user, {}, {}, guid)
                task.put(json.dumps({"user": user, "create": creation_time, "station": station,
                                     "data": {"a0": float(a0), "a1": float(a1), "a2": float(a2), "a3": float(a3),
                                              "a4": float(a4), "a5": float(a5), "b0": float(b0), "b1": float(b1),
                                              "b2": float(b2), "b3": float(b3), "b4": float(b4), "b5": float(b5)},
                                     "guid": guid}))
                print('put data to task')
                # block send to port
                #count = int(words[1])
                #if count > 0 and count < 100:

            except Exception as ex:
                print("%s" % ex)
                print("error send - {0}".format(message))

    def onClose(self, wasClean, code, reason):
        clients.pop(self.peer, None)
        print("WebSocket connection closed: {0}".format(reason))

    def sendJSONmsg(self, message, isBinary):
        self.sendMessage(message.encode('utf-8'), isBinary)


def getData(_self, isBinary):
    """

    :return:
    """
    if len(threads) == 1:
        data = {}
        arr_all = {"g1": [], "g2": [], "g3": [], "g4": [], "g5": []}
        count = 0
        run = False
        print('start')
        while True:
            try:
                # read data from serial port
                print(ser.readline().decode('utf-8').split(','))
                arr = ser.readline().decode('utf-8').split(',')
                if count == 250:
                    stations_modeltask_service.update_in_db(datetime.now().strftime('%d.%m.%Y %H:%M:%S'),
                                     {'fields': ['x', 'y'], 'data': arr_all}, data.get('data'), data.get('guid'), 2)
                    arr_all = {"g1": [], "g2": [], "g3": [], "g4": [], "g5": []}
                    count = 0
                    data = {}
                    run = False
                    print('stop')
                    ser.write(bytes('ref;', 'utf-8'))
                    time.sleep(1)
                    ser.close()
                    time.sleep(1)
                    ser.open()
                    print('close-open')
                if run:
                    if len(arr) > 3:
                        y1 = float(arr[0])
                        y2 = float(arr[1])
                        y3 = float(arr[2])
                        y4 = float(arr[3])
                        y5 = float(arr[4])
                        arr = [y1, y2, y3, y4, y5]
                        arr_all["g1"].append([count, y1])
                        arr_all["g2"].append([count, y2])
                        arr_all["g3"].append([count, y3])
                        arr_all["g4"].append([count, y4])
                        arr_all["g5"].append([count, y5])
                        arr_n = []
                        i = 0
                        while i < 5:
                            arr_n.append(float(arr[i]))
                            i += 1
                        for key, value in clients.items():
                            value.sendJSONmsg(str({"data": arr_n, "user": data.get('user')}), isBinary)
                        count += 1
                if count == 0:
                    time.sleep(30)
                    data = task.get()
                    print('get data from task')
                    if data:
                        data = data.decode(encoding='utf-8')
                        data = json.loads(data)
                        a0 = data.get('data', {}).get('a0', 0)
                        a1 = data.get('data', {}).get('a1', 0)
                        a2 = data.get('data', {}).get('a2', 0)
                        a3 = data.get('data', {}).get('a3', 0)
                        a4 = data.get('data', {}).get('a4', 0)
                        a5 = data.get('data', {}).get('a5', 0)
                        b0 = data.get('data', {}).get('b0', 0)
                        b1 = data.get('data', {}).get('b1', 0)
                        b2 = data.get('data', {}).get('b2', 0)
                        b3 = data.get('data', {}).get('b3', 0)
                        b4 = data.get('data', {}).get('b4', 0)
                        b5 = data.get('data', {}).get('b5', 0)
                        ser.write(bytes('a{0};'.format(b0), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('b{0};'.format(b1), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('c{0};'.format(b2), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('d{0};'.format(b3), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('e{0};'.format(b4), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('f{0};'.format(b5), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('g{0};'.format(a0), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('h{0};'.format(a1), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('i{0};'.format(a2), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('j{0};'.format(a3), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('k{0};'.format(a4), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('l{0};'.format(a5), 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('reg1;', 'utf-8'))
                        time.sleep(0.04)
                        ser.write(bytes('start;', 'utf-8'))
                        print('send commands')
                        run = True

            except Exception as ex:
                print('error receive data - %s', ser.readline().decode('utf-8'))
                print('%s', ex)


if __name__ == '__main__':

    try:
        import asyncio
    except ImportError:
        # Trollius >= 0.3 was renamed
        # import trollius as asyncio
        print('import asyncio failed')

    factory = WebSocketServerFactory("ws://localhost:6544")
    factory.protocol = MyServerProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, 'localhost', 6544)
    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()
