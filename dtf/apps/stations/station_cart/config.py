# -*- coding: utf-8 -*-

import configparser

__author__ = 'artem'

config = configparser.ConfigParser()
config._interpolation = configparser.ExtendedInterpolation()
config.read(encoding='utf-8', filenames=['config.ini'])

serial_config = config['SERIAL.CONFIG']
redis_config = config['REDIS.CONFIG']
db_config = config['DB.CONFIG']
