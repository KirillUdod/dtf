from datetime import datetime
import uuid

from django.contrib.postgres.fields import JSONField
from django.db import models
from stdimage import StdImageField
from stdimage.utils import UploadToUUID
from accounts.models import Account


# Create your models here.


class Station(models.Model):
    name = models.CharField(u'Station name', max_length=50)
    description = models.TextField(u'Station\'s description')
    start_data = JSONField(u'Start data for model', default=list([]), blank=True, null=True)
    duration = models.FloatField(u'Duration of implementation')
    image = StdImageField(u'Station\'s photo', upload_to=UploadToUUID(path='stations'), blank=True,
                          variations={'info': {"width": 1024, "height": 766, "crop": True},
                                      'small': {"width": 584, "height": 437, "crop": True}})

    guid = models.UUIDField(default=uuid.uuid4, editable=False)

    class Meta:
        verbose_name = u'Station'
        verbose_name_plural = u'Stations'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def get_url(self):
        url = "/station/info/%s" % self.guid
        return url



class ModelTask(models.Model):

    STATUS_CHOICES = (
        (1, "At the queue"),
        (2, "Done"),
        (3, "Canceled"),
    )
    creation_time = models.DateTimeField(u'Created at')
    finish_time = models.DateTimeField(u'Finished at', blank=True, null=True)
    results_data = JSONField(u'Results array', default=list([]), blank=True, null=True)

    status = models.IntegerField(u'Status', choices=STATUS_CHOICES, default=1)

    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)

    guid = models.UUIDField(default=uuid.uuid4, editable=False)

    class Meta:
        verbose_name = u'Result'
        verbose_name_plural = u'Results'

    def __str__(self):
        return "Made by %s" % self.get_user()

    def __unicode__(self):
        return "Made by %s" % self.get_user()

    def get_user(self):
        return self.user.get_full_name()

    def get_guid(self):
        return str(self.guid)

    def save(self, *args, **kwargs):
        if not(self.creation_time):
            self.creation_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        super(ModelTask, self).save(*args, **kwargs)