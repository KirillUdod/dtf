from django import forms
from django.contrib import admin

from stations.models import Station, ModelTask

from ckeditor.widgets import CKEditorWidget


class DescriptionAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Station
        fields = '__all__'


class StationAdmin(admin.ModelAdmin):
    form = DescriptionAdminForm
    list_display = (u'name', u'description')
    exclude = ('start_data',)


class ModelResultAdmin(admin.ModelAdmin):
    list_display = (u'id', u'get_user', u'guid')
    readonly_fields = (u'creation_time', u'finish_time', u'results_data')


admin.site.register(Station, StationAdmin)
admin.site.register(ModelTask, ModelResultAdmin)
