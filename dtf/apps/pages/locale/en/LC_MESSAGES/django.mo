��          t      �         �    
   �     �  (   �               '     /     C  *   _    �  l  �            5   (     ^     f     n     t     �  %   �                             
          	                     Die erste Deutsch-Technische Fakult&auml;t (DTF)wurde 1992 an der Donezker Nationalen Technischen Universit&auml;t (DonNTU) in Kooperation mit der  Otto-von-Guericke Universit&auml;t Magdeburg (OVGU) gegr&uuml;ndet. Wichtige Faktoren waren hierbei die bereits seit mehreren Jahrzehnten bestehende Partnerschaft zwischen der DonNTU und der OVGU und die Unterst&uuml;tzung seitens der Firma Siemens und des Deutschen Akademischen Austauschdienstes (DAAD). GESCHICHTE Geschichte. Grenzenloses lernen, lehren und forschen KONTAKT PARTNER STUDIUM Unsere Kontaktdaten Unsere ukrainischen Partner Verbund Deutsch-Technische Fakult&auml;ten Project-Id-Version: April2017
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-14 19:12+0100
PO-Revision-Date: 2017-04-19 10:32+0200
Last-Translator: stefan palis
Language-Team: ovgu
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 The first German-Technical Faculty (GTF) has been founded 1992 at the Donezk National Technical University (DonNTU) in cooperation with the Otto-von-Guericke University Magdeburg (OVGU). Important factors have been the partnership between the DonNTU and the OVGU, which has been lasting over several decades, and the support from the Siemens company and the DAAD.  HISTORY HISTORY. Learning, teaching and doing research without borders CONTACT PARTNER STUDY Our Contact Our ukrainian partners Network of German-Technical Faculties 