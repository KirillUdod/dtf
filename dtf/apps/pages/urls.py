from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from pages import views

#urlpatterns = [url(r'^$', views.index, name='index'),
#               url(r'^study$',views.study, name='study'),
#               url(r'^history$',views.history, name='history'),
#               url(r'^ovgu$',views.ovgu, name='ovgu'),
#               url(r'^contact$',views.contact, name='contact'),
#]
urlpatterns = []
urlpatterns += i18n_patterns(

    url(r'^', views.index, name='index'),
    url(r'^study$', views.study, name='study'),
    url(r'^concept$', views.concept, name='concept'),
    url(r'^ovgu$', views.ovgu, name='ovgu'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^drive$', views.drive, name='drive'),
    url(r'^dpi$', views.drive, name='dpi'),
    url(r'^kpi$', views.drive, name='kpi'),
    url(r'^khpi$', views.drive, name='khpi'),
)
