from django.shortcuts import render


def index(request):
    return render(request, 'pages/home.html')

def study(request):
    return render(request, 'pages/study.html')

def concept(request):
    return render(request, 'pages/concept.html')

def ovgu(request):
    return render(request, 'pages/ovgu.html')

def contact(request):
    return render(request, 'pages/contact.html')

def drive(request):
    return render(request, 'pages/drive.html')

def crane(request):
    return render(request, 'pages/crane.html')

def labratory(request):
    return render(request, 'pages/labratory.html')

def studentlab(request):
    return render(request, 'pages/studentlab.html')

def oilgas(request):
    return render(request, 'pages/oilgas.html')

def dpi(request):
    return render(request, 'pages/dpi.html')

def kpi(request):
    return render(request, 'pages/kpi.html')

def khpi(request):
    return render(request, 'pages/khpi.html')
