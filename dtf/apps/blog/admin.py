from django import forms
from django.contrib import admin

from blog.models import Blog

from ckeditor.widgets import CKEditorWidget


class TextAdminForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Blog
        fields = '__all__'


class BlogAdmin(admin.ModelAdmin):
    form = TextAdminForm
    list_display = (u'title', u'create_date', u'publicated', u'author')
    exclude = (u'author', u'slug')

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user.account
        obj.save()

admin.site.register(Blog, BlogAdmin)