from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import permalink
from django.template.defaultfilters import slugify

from stdimage import StdImageField
from stdimage.utils import UploadToUUID

from accounts.models import Account

User = get_user_model()


class Blog(models.Model):
    author = models.ForeignKey(Account, related_name='created_by', blank=False, null=False)

    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    text = models.TextField(u'Text', blank=False, null=False)
    create_date = models.DateField(u'Arrival', auto_now_add=True)
    publicated = models.BooleanField(u'Publicated?', default=False)

    image = StdImageField(u'Station\'s photo', upload_to=UploadToUUID(path='blog'), blank=True,
                          variations={'small': {"width": 152, "height": 152, "crop": True},
                                      'detail': {"height": 400, "width": 920,"crop": True}})

    class Meta:
        verbose_name = u'Blog'
        verbose_name_plural = u'Blog'

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    @permalink
    def get_absolute_url(self):
        return (u'blog-detail', None, { 'slug': self.slug })


    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Blog, self).save(*args, **kwargs)