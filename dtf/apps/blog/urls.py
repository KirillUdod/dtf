# coding: utf-8

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.conf.urls.i18n import i18n_patterns

from blog.views import BlogView, BlogDetailView

urlpatterns = []
urlpatterns += i18n_patterns(
    url(r'^blog/$', BlogView.as_view(), name=u'blog'),
    url(r'^blog/(?P<slug>[\w-]+)/$', BlogDetailView.as_view(), name=u'blog-detail'),
)
