from django.views.generic import TemplateView, View, ListView
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import redirect, reverse
from django.core.exceptions import ObjectDoesNotExist

from blog.models import Blog


class BlogView(ListView):
    model = Blog
    template_name = u'blog/blog_list.html'
    paginate_by = 2
    context_object_name = "blog"

    def get_queryset(self, **kwargs):
        # context = super(BlogView, self).get_context_data(**kwargs)
        res = []
        for blog in Blog.objects.filter(publicated=True).order_by('-id'):
            text = ' '.join(word for word in blog.text.split(' ')[:100])[:-4]
            # if text[-1] == '.':
            #     text = text[:-1]
            res.append({"title": blog.title,
                         "text": text,
                         "create_date": blog.create_date,
                         "author": blog.author,
                         "url": blog.get_absolute_url(),
                         "image": blog.image})
            # print(blog.image.detail)
        return res


class BlogDetailView(TemplateView):
    template_name = u'blog/blog_detail.html'

    def get_context_data(self,slug, **kwargs):
        # print(slug)
        context = super(BlogDetailView, self).get_context_data(**kwargs)
        context[u'blog'] = Blog.objects.filter(publicated=True, slug=slug).first()
        # print(context[u'blog'])
        return context