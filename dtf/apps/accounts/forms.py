from django import forms
from django.contrib.auth import get_user_model, authenticate

from accounts.models import Account, University, UniversityDegree

User = get_user_model()


class RegistrationForm(forms.Form):
    first_name = forms.CharField(label=u'Имя', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'
    }))

    last_name = forms.CharField(label=u'Фамилия', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'
    }))

    middle_name = forms.CharField(label=u'Отчество', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'
    }))

    birthday = forms.DateField(label=u'Дата рождения', required=False, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control datepicker'
    }))

    city = forms.CharField(label=u'Город', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'
    }))

    university = forms.ModelChoiceField(label=u'Uni', required=True, queryset=University.objects.all(),
                                        widget=forms.Select(attrs={'class':'form-control'}))
    degree = forms.ModelChoiceField(label=u'Degree', required=True, queryset=UniversityDegree.objects.all(),
                                    widget=forms.Select(attrs={'class':'form-control'}))

    email = forms.EmailField(label=u'Электронная почта', required=True, widget=forms.EmailInput(attrs={
        u'type': u'email',
        u'class': u'form-control'
    }))

    password = forms.CharField(label=u'Пароль', required=True, widget=forms.PasswordInput(attrs={
        u'type': u'password',
        u'class': u'form-control'
    }))

    def clean_email(self):
        if User.objects.filter(email__iexact=self.cleaned_data[u'email']).exists():
            raise forms.ValidationError(u'Данный почтовый ящик уже используется')
        return self.cleaned_data[u'email']

    def clean_password(self):
        password = self.cleaned_data.get(u'password', u'')
        if not len(password) >= 8:
            raise forms.ValidationError(u'Password should have 8 or more symbols')
        if password.isspace() or len(password.strip()) != len(password):
            raise forms.ValidationError(u'Пароль не может состоять из пробелов')
        return password


class LoginForm(forms.Form):
    email = forms.EmailField(label=u'Электронная почта', required=True, widget=forms.EmailInput(attrs={
        u'type': u'email',
        u'class': u'form-control col-sm-10'
    }))
    password = forms.CharField(label=u'Пароль', required=True, widget=forms.PasswordInput(attrs={
        u'type': u'password',
        u'class': u'form-control col-sm-10'
    }))
    remember_me = forms.BooleanField(label=u'Запомнить меня', required=False, widget=forms.CheckboxInput)

    def clean(self):
        username = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if not user or not user.is_active:
            raise forms.ValidationError("Sorry, that login was invalid. Please try again.")
        return self.cleaned_data

    def login(self, request):
        username = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user


class ProfileSettingsForm(forms.ModelForm):
    first_name = forms.CharField(label=u'Имя', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'
    }))

    last_name = forms.CharField(label=u'Фамилия', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'

    }))

    middle_name = forms.CharField(label=u'Отчество', required=False, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'
    }))

    birthday = forms.DateField(label=u'Дата рождения', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control datepicker'
    }))

    city = forms.CharField(label=u'Город', required=True, widget=forms.TextInput(attrs={
        u'type': u'text',
        u'class': u'form-control'
    }))

    university = forms.ModelChoiceField(label=u'Uni', required=True, queryset=University.objects.all(),
                                        widget=forms.Select(attrs={'class': 'form-control'}))
    degree = forms.ModelChoiceField(label=u'Degree', required=True, queryset=UniversityDegree.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control'}))

    photo = forms.ImageField(label=u'Photo', required=False, widget=forms.FileInput(attrs={'class': 'form-control'}))
    delete_photo = forms.BooleanField(label=u'Delete Your photo?', required=False,
                                      widget=forms.CheckboxInput(attrs={'class': ''}))

    class Meta:
        model = Account
        fields = (u'first_name', u'last_name', u'middle_name', u'city', u'birthday', u'university', u'degree', u'photo')

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ProfileSettingsForm, self).__init__(*args, **kwargs)


class VisitForm(forms.Form):

    recipient = forms.ModelChoiceField(label=u'recipient', required=True,
                                       queryset=Account.objects.filter(is_recipient=True),
                                       widget=forms.Select(attrs={u'class': u'form-control'}))
    university = forms.ModelChoiceField(label=u'Uni', required=True, queryset=University.objects.all(),
                                        widget=forms.Select(attrs={u'class': u'form-control'}))
    from_date = forms.DateField(label=u'From', required=False, widget=forms.DateInput(attrs={
        u'type': u'text',
        u'class': u'form-control datepicker',
    }))
    to_date = forms.DateField(label=u'To', required=False, widget=forms.DateInput(attrs={
        u'type': u'text',
        u'class': u'form-control datepicker',
    }))
