from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from stdimage import StdImageField
from stdimage.utils import UploadToUUID

from .validators import validate_file_extension

User = get_user_model()
# Create your models here.


class AccountManager(models.Manager):
    def create_account(self, user, first_name, last_name, middle_name, birthday, city, university, degree, photo):
        account = self.model(user=user, first_name=first_name, last_name=last_name, middle_name=middle_name,
                             birthday=birthday, city=city, university=university, degree=degree, photo=photo)
        account.save(using=self._db)
        return account


class UniversityDegree(models.Model):
    degree = models.CharField(u'Degree', max_length=50)
    comment = models.CharField(u'Comments', max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = u'Degree'
        verbose_name_plural = u'Degrees'

    def __str__(self):
        return self.degree

    def __unicode__(self):
        return self.degree


class University(models.Model):
    name = models.CharField(u'Full name', max_length=255)
    short_name = models.CharField(u'Short name', max_length=10)
    address = models.CharField(u'Address', max_length=255)

    class Meta:
        verbose_name = u'University'
        verbose_name_plural = u'Universities'

    def __str__(self):
        return self.short_name

    def __unicode__(self):
        return self.short_name


class Account(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=u'Пользователь', related_name=u'account')

    first_name = models.CharField(u'First Name', max_length=255)
    last_name = models.CharField(u'Last Name', max_length=255)
    middle_name = models.CharField(u'Patronymic', max_length=255, blank=True, null=True)

    city = models.CharField(u'City', max_length=255, blank=True, null=True)
    birthday = models.DateField(u'Birthday', blank=True, null=True)
    university = models.ForeignKey(University, on_delete=models.CASCADE, blank=True, null=True)
    degree = models.ForeignKey(UniversityDegree, on_delete=models.CASCADE, blank=True, null=True)

    photo = StdImageField(u'Photo', upload_to=UploadToUUID(path='users'), blank=True,
                          variations={'avatar': {"width": 219, "height": 292, "crop": True}})

    is_recipient = models.BooleanField(u'Is Recipient', default=False)

    objects = AccountManager()

    class Meta:
        verbose_name = u'Account'
        verbose_name_plural = u'Accounts'

    def __str__(self):
        return self.get_full_name()

    def __unicode__(self):
        return self.get_full_name()

    def get_full_name(self):
        full_name = u'%s %s' % (self.last_name, self.first_name)
        if self.middle_name:
            full_name = u'%s %s' % (full_name, self.middle_name)
        return full_name.strip()

    def get_email(self):
        return self.user.email

    def is_active(self):
        return self.user.is_active

    def save(self, *args, **kwargs):
        User.objects.filter(username=self.user.username).update(username=self.user.username.lower())
        # if not(self.user.email): self.user.email = self.user.username
        # print(self.user)
        # user = Account.objects.get(user=self.user)
        # self.photo = open('./static/pages/img/avatar.jpg', 'r')
        super(Account, self).save(*args, **kwargs)


class Visit(models.Model):
    # FIXME: пересмотреть названия
    user = models.ForeignKey(Account, related_name='to_user', blank=False, null=False)

    recipient = models.ForeignKey(Account,
                                  related_name='recipient',
                                  limit_choices_to={'is_recipient': True},
                                  blank=False,
                                  null=False)
    university = models.ForeignKey(University, related_name='university', blank=True, null=True)
    from_date = models.DateField(u'From', blank=True, null=True)
    to_date = models.DateField(u'To', blank=True, null=True)
    arrival_date = models.DateField(u'Arrival', blank=True, null=True)
    departure_date = models.DateField(u'Departure', blank=True, null=True)
    invitation = models.FileField(u'Invitation Doc', blank=True, null=True,
                                  validators=[validate_file_extension],
                                  upload_to=UploadToUUID(path='invitations'))

    approved = models.BooleanField(u'Approved', default=False)

    def save(self, *args, **kwargs):
        if self.invitation:
            self.approved = True
        else:
            self.approved = False
        super(Visit, self).save(*args, **kwargs)

    def __str__(self):
        return "%s to %s from %s" % (self.user, self.recipient, self.from_date)

    def __unicode__(self):
        return "%s to %s from %s" % (self.user, self.recipient, self.from_date)

# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Account.objects.create(user=instance)
#
# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def save_user_profile(sender, instance, **kwargs):
#     instance.account.save()

