# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-05 12:24
from __future__ import unicode_literals

from django.db import migrations
import stdimage.models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_account_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='photo',
            field=stdimage.models.StdImageField(blank=True, upload_to='users', verbose_name='Photo'),
        ),
    ]
