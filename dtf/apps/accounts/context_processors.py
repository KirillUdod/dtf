from accounts.forms import VisitForm, LoginForm, RegistrationForm


def auth_forms(request):
    return {
        u'login_form': LoginForm(),
        u'registration_form': RegistrationForm(),
        u'add_visit_form': VisitForm()
    }