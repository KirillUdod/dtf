from django.contrib.auth import get_user_model, login, logout
from django.core.urlresolvers import reverse
from django.db import transaction
from django.shortcuts import redirect
from django.views.generic import FormView, View, TemplateView

from accounts.forms import RegistrationForm, LoginForm, VisitForm, ProfileSettingsForm
from accounts.models import Account, Visit
from stations.models import ModelTask

User = get_user_model()


class AjaxTemplateMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'ajax_template_name'):
            split = self.template_name.split('.html')
            split[-1] = '_inner'
            split.append('.html')
            self.ajax_template_name = ''.join(split)
        if request.is_ajax():
            self.template_name = self.ajax_template_name
        return super(AjaxTemplateMixin, self).dispatch(request, *args, **kwargs)


class RegistrationView(FormView):
    form_class = RegistrationForm
    template_name = u'accounts/registration.html'

    @transaction.atomic
    def try_to_create_user(self, email, password, first_name, last_name, middle_name, birthday, city, university, degree):
        user = User.objects.create_user(username=email, email=email, password=password)
        user.is_active = True
        user.save()
        Account.objects.create_account(
            user=user,
            first_name=first_name,
            last_name=last_name,
            middle_name=middle_name,
            birthday=birthday,
            city=city,
            university=university,
            degree=degree
        )
        if user:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(self.request, user)
            return redirect(reverse(u'profile'))
        else:
            return redirect(reverse(u'index'))

    def form_valid(self, form):
        email, password, first_name, last_name, middle_name, birthday, city, university, degree = (
            form.cleaned_data[u'email'],
            form.cleaned_data[u'password'],
            form.cleaned_data[u'first_name'],
            form.cleaned_data[u'last_name'],
            form.cleaned_data[u'middle_name'],
            form.cleaned_data[u'birthday'],
            form.cleaned_data[u'city'],
            form.cleaned_data[u'university'],
            form.cleaned_data[u'degree'],
        )
        self.try_to_create_user(email, password, first_name, last_name, middle_name,
                                birthday, city, university, degree)
        return redirect(reverse(u'index'))

    # def form_invalid(self, form):
    #     # TODO: last step
    #     print('zzzzzzzzzzz')


class LoginView(FormView):

    template_name = u'accounts/login.html'
    form_class = LoginForm

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(reverse(u'profile'))
        return super(LoginView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        if not form.cleaned_data[u'remember_me']:
            self.request.session.set_expiry(0)
        user = form.login(self.request)
        if user:
            login(self.request, user)
            return redirect(reverse(u'profile'))
        else:
            return redirect(reverse(u'login'))

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        return context


class LogOut(View):
    def get(self, request):
        return self.post(request)

    def post(self, request):
        logout(request)
        return redirect(reverse(u'index'))


class ProfileView(TemplateView):
    template_name = u'accounts/profile/profile.html'

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self.request.user, u'account'):
            return redirect(reverse(u'index'))
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context[u'account'] = self.request.user.account
        context[u'models'] = ModelTask.objects.filter(user=self.request.user.account.id)
        context[u'visits'] = Visit.objects.filter(user=self.request.user.account.id,).order_by('-id')
        return context


class ProfileSettingsView(FormView):
    template_name = u'accounts/profile/profile_settings.html'
    form_class = ProfileSettingsForm

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self.request.user, u'account'):
            return redirect(reverse(u'index'))
        return super(ProfileSettingsView, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        self.user = self.request.user
        self.account = self.user.account
        return {
            u'first_name': self.account.first_name,
            u'last_name': self.account.last_name,
            u'middle_name': self.account.middle_name,
            u'birthday': self.account.birthday,
            u'city': self.account.city,
            u'university': self.account.university,
            u'degree': self.account.degree,
        }

    def get_context_data(self, **kwargs):
        context = super(ProfileSettingsView, self).get_context_data(**kwargs)
        context[u'photo'] = self.request.user.account.photo
        return context


    def get_form_kwargs(self):
        kwargs = super(ProfileSettingsView, self).get_form_kwargs()
        kwargs.update({u'user': self.user})
        return kwargs

    def form_valid(self, form):
        first_name, last_name, middle_name, birthday, city, university, degree, delete_photo = (
            form.cleaned_data.get(u'first_name'),
            form.cleaned_data.get(u'last_name'),
            form.cleaned_data.get(u'middle_name'),
            form.cleaned_data.get(u'birthday'),
            form.cleaned_data.get(u'city'),
            form.cleaned_data.get(u'university'),
            form.cleaned_data.get(u'degree'),
            form.cleaned_data.get(u'delete_photo'),
        )
        self.account.first_name = first_name
        self.account.last_name = last_name
        self.account.middle_name = middle_name
        self.account.city = city
        self.account.birthday = birthday
        self.account.university = university
        self.account.degree = degree
        if 'photo' in self.request.FILES:
                self.account.photo = self.request.FILES['photo']
        if delete_photo:
            self.account.photo = None
        self.account.save()
        return redirect(reverse(u'profile'))


class AddVisitView(AjaxTemplateMixin, FormView):
    template_name = u'accounts/profile/add_visit.html'
    form_class = VisitForm

    def form_valid(self, form):
        new_visit = Visit(user=self.request.user.account,
                          recipient=form.cleaned_data['recipient'],
                          university=form.cleaned_data['university'],
                          from_date=form.cleaned_data['from_date'],
                          to_date=form.cleaned_data['to_date']
                          )
        new_visit.save()
        return redirect(u'profile')