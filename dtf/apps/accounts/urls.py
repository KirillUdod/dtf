# coding: utf-8

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.conf.urls.i18n import i18n_patterns

from accounts.views import LoginView, LogOut, RegistrationView, ProfileView, AddVisitView, ProfileSettingsView
from stations.views import StationsView, ShortStationInfoView, StationInfoView, ModelingView, ResultsView, \
    GetPDFResultsView, GetExcelResultsView

urlpatterns = []
urlpatterns += i18n_patterns(
    url(r'^registration/$', RegistrationView.as_view(), name=u'registration'),
    url(r'^login/$', LoginView.as_view(), name=u'login'),
    url(r'^logout/$', LogOut.as_view(), name=u'logout'),
    url(r'^profile/$', login_required(ProfileView.as_view()), name=u'profile'),
    url(r'^profile/settings/$', login_required(ProfileSettingsView.as_view()), name=u'profile_settings'),
    url(r'^stations/$', login_required(StationsView.as_view()), name=u'stations'),
    # url(r'^station/get/$', login_required(get_station)),
    url(r'^station/get/$', login_required(ShortStationInfoView.as_view()), name=u'short_station_info'),
    url(r'^station/info/(?P<guid>[0-9a-f-]+)/$', login_required(StationInfoView.as_view()), name=u'station_info'),
    url(r'^station/modeling/(?P<guid>[0-9a-f-]+)/$', login_required(ModelingView.as_view()), name=u'modeling'),
    # url(r'^station/set_parameters', login_required(SetModelsParametersView.as_view(), new))
    url(r'^add_visit/$', login_required(AddVisitView.as_view()), name=u'add_visit'),
    url(r'^results/$', login_required(ResultsView.as_view()), name=u'results'),
    url(r'get_excel/(?P<guid>[0-9a-f-]+)/$', login_required(GetExcelResultsView.as_view()), name=u'get_excel'),
    url(r'get_pdf/(?P<guid>[0-9a-f-]+)/$', login_required(GetPDFResultsView.as_view()), name=u'get_pdf'),
)
