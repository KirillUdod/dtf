from django.contrib import admin

from accounts.models import Account, University, UniversityDegree, Visit

# Register your models here.
class AccountAdmin(admin.ModelAdmin):
    list_display = (u'get_full_name', u'university', u'is_active')
    list_display_links = (u'get_full_name', u'university')
    # readonly_fields = (,)
    ordering = (u'id',)

    def date_joined(self, instance):
        return instance.user.date_joined

    def is_active(self, instance):
        return instance.user.is_active


class UniversityAdmin(admin.ModelAdmin):
    list_display = (u'name', u'short_name')

class UniversityDegreeAdmin(admin.ModelAdmin):
    list_display = (u'id', u'degree')

class VisitAdmin(admin.ModelAdmin):
    list_display = (u'user',u'recipient', u'approved')
    readonly_fields = [u'approved']

admin.site.register(University, UniversityAdmin)
admin.site.register(UniversityDegree, UniversityDegreeAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Visit, VisitAdmin)