"""dtf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static

from pages import views as pages_views
from accounts.urls import urlpatterns as urlpatterns_accounts
from blog.urls import urlpatterns as urlpatterns_blog

urlpatterns = [

    url(r'^admin/', admin.site.urls),

    #url(r'^accounts/', include(u'accounts.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += i18n_patterns(
    url(r'^$', pages_views.index, name='index'),
    url(r'^study$',pages_views.study, name='study'),
    url(r'^concept$',pages_views.concept, name='concept'),
    url(r'^ovgu$',pages_views.ovgu, name='ovgu'),
    url(r'^contact$',pages_views.contact, name='contact'),
    url(r'^drive$',pages_views.drive, name='drive'),
    url(r'^crane$',pages_views.crane, name='crane'),
    url(r'^labratory$',pages_views.labratory, name='labratory'),
    url(r'^studentlab$', pages_views.studentlab, name='studentlab'),
    url(r'^oilgas$', pages_views.oilgas, name='oilgas'),
    url(r'^dpi$', pages_views.dpi, name='dpi'),
    url(r'^kpi$', pages_views.kpi, name='kpi'),
    url(r'^khpi$', pages_views.khpi, name='khpi'),

) + urlpatterns_accounts\
               + urlpatterns_blog \
               + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
               + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
