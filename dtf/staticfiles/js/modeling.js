/**
 * Created by artem on 07.06.17.
 */
$(function () {

    String.prototype.replaceAll = function (str1, str2, ignore) {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof(str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
    };
    var ws = new WebSocket("ws://localhost:6544");

    var data = {},
        totalPoints = 250,
        _start_modeling = $("#start_modeling"),
        _a0 = $("#a0"),
        _a1 = $("#a1"),
        _a2 = $("#a2"),
        _a3 = $("#a3"),
        _a4 = $("#a4"),
        _a5 = $("#a5"),
        _b0 = $("#b0"),
        _b1 = $("#b1"),
        _b2 = $("#b2"),
        _b3 = $("#b3"),
        _b4 = $("#b4"),
        _b5 = $("#b5"),
        call = false,
        user = $("#user").val(),
        station = $("#station").val(),
        _show_wait = $("#show_wait"),
        _start_mod = $("#start_mod");

    _show_wait.hide();
    _start_mod.hide();

    for (var t = 0;  t < 5; t++){
        var arr = [];
        for (var i = 0; i < totalPoints; ++i) {
            arr.push([i, 0]);
        }
        data[t] = arr;
    }
    // вызовется, когда соединение будет установлено:
    ws.onopen = function() {
        console.log('connect');
    };

    // вызовется, когда соединение закроется
    ws.onclose = function() {
        console.log('close');
    };

    // вызовется, когда браузер получает какие-то данные через веб-сокет
    ws.onmessage = function(evt) {
        _show_wait.hide();
        _start_mod.show();
        var datas = JSON.parse(evt.data.replaceAll("'", '"'));
        if (datas.user == user)
        {
            getData(JSON.parse(evt.data.replaceAll("'", '"')));
        }
    };

    /*_position.bind("slideStop", function (e) {
        console.log(e.value);
        ws.send(JSON.stringify('send_data ' + e.value));
            if (!call)
            {
                ws.send(JSON.stringify('get_data'));
                call = true;
            }
    });*/

    jQuery(window).bind(
        "beforeunload",
        function() {
            message = 'get_data';
            //ws.close();
            //ws.send(JSON.stringify(message));
        }
    );
    // We use an inline data source in the example, usually data would
    // be fetched from a server

    function getData(datas) {

        var i = 0,
            dataNew = {};

        for (var t = 0;  t < 5; t++){
            var arr = [];
            for (var i = 0; i < totalPoints-1; ++i) {
                arr.push([i, data[t][i+1][1]]);
            }
            dataNew[t] = arr;
        }
        for (var t = 0; t < 5; t++){
            var arr = dataNew[t]
            arr.push([totalPoints - 1, datas.data[t]]);
            dataNew[t] = arr;
        }
        data = {};
        data = dataNew;
    }

     // Set up the control widget

     //		var updateInterval = 30;
     //		$("#updateInterval").val(updateInterval).change(function () {
     //			var v = $(this).val();
     //			if (v && !isNaN(+v)) {
     //				updateInterval = +v;
     //				if (updateInterval < 1) {
     //					updateInterval = 1;
     //				} else if (updateInterval > 2000) {
     //					updateInterval = 2000;
     //				}
     //				$(this).val("" + updateInterval);
     //			}
     //		});
     var plot1 = $.plot("#placeholder_1", [{
         data: data[0],
         label: "SP",
         lines:{
             show: true
         }
        }, {
         data: data[0],
         label: "IP",
         lines: {
             show: true
         }
        }], {
            series: {
                shadowSize: 0	// Drawing is faster without shadows
            },
            yaxis: {
                min: -20,
                max: 200
            },
            xaxis: {
                show: true
            }
     });
     var plot2 = $.plot("#placeholder_2", [ data[1] ], {
            series: {
                shadowSize: 0	// Drawing is faster without shadows
            },
            yaxis: {
                min: -40,
                max: 170
            },
            xaxis: {
                show: true
            }
     });
     var plot3 = $.plot("#placeholder_3", [ data[2] ], {
            series: {
                shadowSize: 0	// Drawing is faster without shadows
            },
            yaxis: {
                min: -2500,
                max: 2800
            },
            xaxis: {
                show: true
            }
    });
    var plot4 = $.plot("#placeholder_4", [ data[3] ], {
            series: {
                shadowSize: 0	// Drawing is faster without shadows
            },
            yaxis: {
                min: -2500,
                max: 2500
            },
            xaxis: {
                show: true
            }
    });


    function update() {
        plot1.setData([{
         data: data[0],
         label: "SP",
         lines:{
             show: true
         }
        },
        {
         data: data[1],
         label: "IP",
         lines:{
             show: true
         }
        }]);
        plot2.setData([data[2]]);
        plot3.setData([data[3]]);
        plot4.setData([data[4]]);

        // Since the axes don't change, we don't need to call plot.setupGrid()
        plot1.draw();
        plot2.draw();
        plot3.draw();
        plot4.draw();
        setTimeout(update, 40);
    }

    update();

    _start_modeling.bind("click", function (e){
        e.preventDefault();
        var s = user + " " + _a0.val() + " " + _a1.val() + " " + _a2.val() + " " + _a3.val() + " " + _a4.val() + " " +
                + _a5.val() + " " + _b0.val() + " " + _b1.val() + " " + _b2.val() + " " + _b3.val() + " " +
                + _b4.val() + " " + _b5.val() + " " + station;
        ws.send(JSON.stringify('send_data ' + s));
        //if (!call)
        //{
        //    console.log('get')
        //    ws.send(JSON.stringify('get_data'));
        //    call = true;
        //}
        _show_wait.show();
        _start_mod.hide();
    });

});
