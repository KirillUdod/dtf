

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'dtf',  # Or path to database file if using sqlite3.
        'USER': 'dtf',  # Not used with sqlite3.
        'PASSWORD': 'dtf2017',  # Not used with sqlite3.
        'HOST': u'localhost',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': u'5432',  # Set to empty string for default. Not used with sqlite3.
    }
}